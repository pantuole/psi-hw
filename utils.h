#include "vector"
#include "stdexcept"
#include "constants.h"

#ifndef BIE_PSI_UTILS_H
#define BIE_PSI_UTILS_H

struct Coords {
    int x, y;

    explicit Coords(int init_x = INT32_MIN, int init_y = INT32_MIN) {
        x = init_x;
        y = init_y;
    }
};

inline int to_16_bit(int num) {
    return num % 65536;
}

bool is_integer(double num);

bool is_numeric(const string &number);

string pop_from_buffer(const char *buffer, size_t buffer_size);

Coords resolve_position(const string &message);

#endif //BIE_PSI_UTILS_H
