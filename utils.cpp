#include "utils.h"
#include "algorithm"

string pop_from_buffer(const char *buffer, size_t buffer_size) {
    string str;
    for (size_t i = 0; i < buffer_size - 1; i++) {
        if (buffer[i] == END_SIGNAL[0] &&
            buffer[i + 1] == END_SIGNAL[1]) {
            return str;
        }
        str.push_back(buffer[i]);
    }
    str.push_back(buffer[buffer_size - 1]);
    return str;
}

bool is_integer(double num) {
    return (num - (double) (long) num) == 0;
}

bool is_numeric(const string &number) {
    return all_of(number.cbegin(), number.cend(), [](int i) { return i >= '0' && i <= '9'; });
}

vector<string> split(const string &str, const string &separator) {
    vector<string> parts;
    size_t start, end = -1 * separator.size();
    do {
        start = end + separator.size();
        end = str.find(separator, start);
        parts.push_back(str.substr(start, end - start));
    } while (end != (size_t) -1);
    return parts;
}

Coords resolve_position(const string &message) {
    vector<string> words = split(message, " ");
    if (words.size() != 3) {
        throw invalid_argument("Too much words!");
    }
    double x = stod(words[1]);
    double y = stod(words[2]);
    if (!is_integer(x) || !is_integer(y)) {
        throw invalid_argument("Coordinates are not integers!");
    }
    return Coords((int) x, (int) y);
}