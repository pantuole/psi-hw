#include "wait.h"
#include "robot.h"
#include "cstring"
#include "iostream"
#include "unistd.h"
#include "constants.h"
#include "sys/socket.h"

#ifndef BIE_PSI_CONNECTION_H
#define BIE_PSI_CONNECTION_H

using namespace std;

class Connection {
    int c_id;
    int socket;
    fd_set sockets{};
    timeval timeout{};
    RobotStateMachine robot;
    sockaddr remote_address{};
    socklen_t remote_address_length{};

    void stop(const char *message) const {
        perror(message);
        close(c_id);
        exit(-1);
    }

    void check_connection() {
        FD_ZERO(&sockets);
        FD_SET(c_id, &sockets);

        if (select(c_id + 1, &sockets, nullptr, nullptr, &timeout) < 0) {
            //is called before recv of each message, first argument: last FD +1
            stop("Error in select.");
        }
        if (!FD_ISSET(c_id, &sockets)) {
            stop("Connection timeout!");
        }
    }

    void process_buffer(const string &message) {
        do {
            auto response = robot.process(message);
            if (!response.empty()) {
                send(c_id, response.data(), response.size(), 0);
                send(c_id, END_SIGNAL.data(), END_SIGNAL_LEN, 0);
            }
        } while (!robot.wait_next_message() && robot.state != RobotState::ERROR);
        timeout.tv_sec = robot.get_timeout();
    }

    void read_data() {
        size_t buffer_offset = 0;
        auto *buffer = new char[BUFFER_SIZE];

        while (true) {
            check_connection();

            auto bytes_received = recv(c_id, buffer + buffer_offset, BUFFER_SIZE - 1, 0);
            timeout.tv_sec = 1;

            if (bytes_received < 0) {
                stop("Can't read.");
            }
            if (bytes_received == 0) {
                cout << "Connection closed" << endl;
                break;
            }

            auto buffer_size = buffer_offset + bytes_received;

            string message = pop_from_buffer(buffer, buffer_size);

            if (!robot.validate_input(message)) {
                send(c_id, SERVER_SYNTAX_ERROR.data(), SERVER_SYNTAX_ERROR.size(), 0);
                send(c_id, END_SIGNAL.data(), END_SIGNAL_LEN, 0);
                close(c_id);
                break;
            }

            // skip if end signal was not found
            if (buffer_size < END_SIGNAL_LEN || message.size() == buffer_size) {
                buffer_offset += bytes_received;
                continue;
            }

            size_t chars_read = 0, chars_left;

            // process buffer divided into messages
            do {
                chars_read += message.size() + END_SIGNAL_LEN;
                chars_left = buffer_size - chars_read;
                process_buffer(message);
                if (chars_left < 3) break;
                message = pop_from_buffer(buffer + chars_read, chars_left);
            } while (message.size() != chars_left);

            if (chars_left) {
                // move left data to the start
                memcpy(buffer, buffer + chars_read, chars_left);
                memset(buffer + chars_left, '\0', BUFFER_SIZE - chars_left);
            }

            if (robot.session_ended()) break;

            buffer_offset = chars_left;
        }

        delete[] buffer;
    }

public:
    explicit Connection(int socket) {
        c_id = accept(socket, &remote_address, &remote_address_length);
        if (c_id < 0) {
            perror("Can't accept new connection.");
            return;
        }
        timeout.tv_sec = 60;
        timeout.tv_usec = 1;
        this->socket = socket;
    }

    void listen() {
        if (c_id < 0) return;

        pid_t pid = fork(); //copy of actual process

        if (pid == 0) { //copy of process, does not have child
            close(socket); //do not need listening socket in the copy
            read_data();
            close(c_id);
            exit(0);
        }

        waitpid(0, nullptr, WNOHANG);
        close(c_id);
    }
};

#endif //BIE_PSI_CONNECTION_H
