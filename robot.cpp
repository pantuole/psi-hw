#include "robot.h"
#include "constants.h"

string RobotStateMachine::set_username() {
    robot.username = message;
    state = RobotState::GENERATE_HASH;
    return SERVER_KEY_REQUEST;
}

string RobotStateMachine::generate_hash() {
    robot.auth_key = stoi(message);
    if (robot.auth_key < 0 || robot.auth_key > 4) {
        state = RobotState::ERROR;
        return SERVER_KEY_OUT_OF_RANGE_ERROR;
    }
    robot.calculate_hash();
    auto server_key = AUTH_KEYS[robot.auth_key].first;
    auto confirm_key = to_16_bit(robot.hash + server_key);
    state = RobotState::CHECK_ROBOT_CLIENT;
    return to_string(confirm_key);
}

string RobotStateMachine::check_robot_client() {
    auto client_confirm_key = stoi(message);
    auto client_key = AUTH_KEYS[robot.auth_key].second;
    auto expected_confirm_key = to_16_bit(robot.hash + client_key);
    if (client_confirm_key == expected_confirm_key) {
        state = RobotState::CHECK_START_POSITION;
        wait_for_message = false;
        return SERVER_OK;
    } else {
        state = RobotState::ERROR;
        return SERVER_LOGIN_FAILED;
    }
}

string RobotStateMachine::obstacle_found() {
    state = RobotState::MOVE_OVER_OBSTACLE;
    robot.obstacle_state = 0;
    wait_for_message = false;
    return "";
}

string RobotStateMachine::check_orientation() {
    if (robot.prev_position.x == INT32_MIN && robot.prev_position.y == INT32_MIN) {
        robot.prev_position = resolve_position(message);
        return SERVER_MOVE;
    }

    robot.position = resolve_position(message);
    if (!robot.is_moving()) {
        return obstacle_found();
    }

    robot.calculate_orientation();
    robot.position = robot.prev_position;
    state = RobotState::MOVE_TO_TARGET;
    wait_for_message = false;
    return "";
}

string RobotStateMachine::move_to_target() {
    robot.prev_position = robot.position;
    robot.position = resolve_position(message);

    if (prev_state != RobotState::ROTATE && !robot.is_moving()) {
        return obstacle_found();
    }

    if (robot.position.x == 0 && robot.position.y == 0) {
        state = RobotState::PICKUP_SECRET;
        return SERVER_PICK_UP;
    }

    robot.calculate_rotation();

    if (robot.rotate_to != robot.orientation) {
        state = RobotState::ROTATE;
        wait_for_message = false;
        return "";
    }

    return SERVER_MOVE;
}

string RobotStateMachine::rotate() {
    auto right_moves = (int) robot.rotate_to - (int) robot.orientation;
    if (abs(right_moves) == 1) state = RobotState::MOVE_TO_TARGET;
    if (right_moves != -1) {
        robot.turnRight();
        return SERVER_TURN_RIGHT;
    } else {
        robot.turnLeft();
        return SERVER_TURN_LEFT;
    }
}

string RobotStateMachine::move_over_obstacle() {
    if (++robot.obstacle_state == 4) {
        if (robot.orientation == Orientation::UNKNOWN) {
            state = RobotState::CHECK_ORIENTATION;
            robot.reset_position();
        } else {
            state = RobotState::MOVE_TO_TARGET;
        }
    }
    bool right = robot.position.x * robot.position.y > 0;
    return (right
            ? move_over_obstacle_plot_right
            : move_over_obstacle_plot_left)[robot.obstacle_state - 1];
}

string RobotStateMachine::check_start_position() {
    state = RobotState::CHECK_ORIENTATION;
    return SERVER_MOVE;
}

string RobotStateMachine::pickup_secret() {
    state = RobotState::FINISH;
    return SERVER_LOGOUT;
}

string RobotStateMachine::feed() {
    switch (state) {
        case RobotState::START:
            return set_username();
        case RobotState::GENERATE_HASH:
            return generate_hash();
        case RobotState::CHECK_ROBOT_CLIENT:
            return check_robot_client();
        case RobotState::CHECK_START_POSITION:
            return check_start_position();
        case RobotState::CHECK_ORIENTATION:
            return check_orientation();
        case RobotState::MOVE_TO_TARGET:
            return move_to_target();
        case RobotState::ROTATE:
            return rotate();
        case RobotState::MOVE_OVER_OBSTACLE:
            return move_over_obstacle();
        case RobotState::PICKUP_SECRET:
            return pickup_secret();
        case RobotState::ERROR:
        case RobotState::FINISH:
        default:
            return "";
    }
}

string RobotStateMachine::process(const string &input) {
    message = input;
    if (!wait_for_message) {
        wait_for_message = true;
    }
    if (message == CLIENT_RECHARGING) {
        prev_state = state;
        state = RobotState::RECHARGING;
        return "";
    }
    if (message == CLIENT_FULL_POWER) {
        if (state != RobotState::RECHARGING) {
            state = RobotState::ERROR;
            return SERVER_LOGIC_ERROR;
        }
        state = prev_state;
        return "";
    }
    if (state == RobotState::RECHARGING) {
        state = RobotState::ERROR;
        return SERVER_LOGIC_ERROR;
    }
    if (!validate_input(message)) {
        return syntax_error();
    }
    try {
        auto state_before_operation = state;
        string result_message = feed();
        prev_state = state_before_operation;
        return result_message;
    } catch (invalid_argument &err) {
        return syntax_error();
    }
}

string RobotStateMachine::syntax_error() {
    state = RobotState::ERROR;
    return SERVER_SYNTAX_ERROR;
}

bool RobotStateMachine::validate_input(const string &buffer) const {
    if (buffer == CLIENT_RECHARGING) {
        return true;
    }
    // remove '\a' if it found
    string input = buffer.back() == END_SIGNAL[0]
                   ? string(buffer, 0, buffer.size() - 1)
                   : buffer;
    switch (state) {
        case RobotState::START:
            return input.size() <= MAX_USERNAME_LEN;
        case RobotState::GENERATE_HASH:
            return input.size() == 1;
        case RobotState::CHECK_ROBOT_CLIENT:
            return input.size() <= MAX_CONFIRMATION_KEY_LEN && is_numeric(input);
        case RobotState::MOVE_OVER_OBSTACLE:
        case RobotState::CHECK_ORIENTATION:
        case RobotState::MOVE_TO_TARGET:
        case RobotState::RECHARGING:
        case RobotState::ROTATE:
            return input.size() <= MAX_MOVEMENT_MESSAGE_LEN;
        case RobotState::PICKUP_SECRET:
            return input.size() <= MAX_SECRET_MESSAGE_LEN;
        case RobotState::ERROR:
        case RobotState::FINISH:
        case RobotState::CHECK_START_POSITION:
            return true;
    }
    return true;
}
