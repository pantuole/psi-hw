#include "utils.h"

#ifndef BIE_PSI_ROBOT_H
#define BIE_PSI_ROBOT_H

enum class Orientation {
    UP, RIGHT, DOWN, LEFT, UNKNOWN
};

class Robot {
public:
    int hash = 0;
    string username;
    int auth_key = 0;
    int obstacle_state = 0;
    Coords position, prev_position;
    Orientation rotate_to = Orientation::UNKNOWN;
    Orientation orientation = Orientation::UNKNOWN;

    void reset_position() {
        position.x = position.y = prev_position.x = prev_position.y = INT32_MIN;
    }

    void calculate_hash() {
        int hash_sum = 0;
        for (char c: username) hash_sum += (int) c;
        hash = to_16_bit(hash_sum * 1000);
    }

    bool is_moving() const {
        return position.x != prev_position.x ||
               position.y != prev_position.y;
    }

    void calculate_orientation() {
        int dx = position.x - prev_position.x;
        int dy = position.y - prev_position.y;

        if (dx > 0) orientation = Orientation::RIGHT;
        else if (dx < 0) orientation = Orientation::LEFT;
        else if (dy > 0) orientation = Orientation::UP;
        else orientation = Orientation::DOWN;
    }

    void calculate_rotation() {
        rotate_to = position.x != 0
                    ? position.x > 0 ? Orientation::LEFT : Orientation::RIGHT
                    : position.y > 0 ? Orientation::DOWN : Orientation::UP;
    }

    void turnRight() {
        orientation = Orientation(((int) orientation + 1) % 4);
    }

    void turnLeft() {
        orientation = Orientation(((int) orientation - 1) % 4);
    }
};

enum class RobotState {
    START,
    GENERATE_HASH,
    CHECK_ROBOT_CLIENT,
    CHECK_START_POSITION,
    CHECK_ORIENTATION,
    MOVE_TO_TARGET,
    ROTATE,
    MOVE_OVER_OBSTACLE,
    RECHARGING,
    PICKUP_SECRET,
    FINISH,
    ERROR
};

class RobotStateMachine {
    string message;
    Robot robot = Robot();
    bool wait_for_message = true;

    string set_username();

    string generate_hash();

    string check_robot_client();

    string check_start_position();

    string check_orientation();

    string move_to_target();

    string rotate();

    string obstacle_found();

    string move_over_obstacle();

    string pickup_secret();

    string feed();

    string syntax_error();

public:
    RobotState state = RobotState::START;
    RobotState prev_state = RobotState::START;

    int get_timeout() const {
        return state == RobotState::RECHARGING ? 5 : 1;
    }

    bool wait_next_message() const {
        return wait_for_message;
    }

    bool session_ended() const {
        return state == RobotState::ERROR || state == RobotState::FINISH;
    }

    bool validate_input(const string &input) const;

    string process(const string &input);
};

#endif //BIE_PSI_ROBOT_H
