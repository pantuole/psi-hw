cmake_minimum_required(VERSION 3.16)
project(BIE_PSI)

set(CMAKE_CXX_STANDARD 14)

add_executable(BIE_PSI server.cpp robot.cpp utils.cpp)
