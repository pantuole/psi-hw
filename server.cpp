#include "connection.h"
#include "netinet/in.h"

using namespace std;

class Server {
    int socket_id;

public:
    bool started = false;

    Server(long port, int max_clients) {
        sockaddr_in address{};
        address.sin_family = PF_INET;
        address.sin_port = htons(port);
        address.sin_addr.s_addr = htonl(INADDR_ANY);

        socket_id = socket(AF_INET, SOCK_STREAM, 0);

        if (bind(socket_id, (sockaddr *) &address, sizeof(address)) != 0) {
            perror("Problem with bind()");
            close(socket_id);
            return;
        }

        if (listen(socket_id, max_clients) != 0) {
            perror("Can't set socket to listen.");
            close(socket_id);
            return;
        }

        started = true;
    }

    [[noreturn]] void accept_connections() const {
        while (true) {
            Connection(socket_id).listen();
        }
    }

};

int main(__attribute__((unused)) int _, char **argv) {
    auto port = strtol(argv[1], nullptr, 10);
    Server server = Server(port, 10);

    if (!server.started) {
        return 1;
    }

    server.accept_connections();
}
