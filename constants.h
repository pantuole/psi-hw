#include"string"

#ifndef BIE_PSI_CONSTANTS_H
#define BIE_PSI_CONSTANTS_H
using namespace std;

const string SERVER_MOVE = "102 MOVE";
const string SERVER_TURN_LEFT = "103 TURN LEFT";
const string SERVER_TURN_RIGHT = "104 TURN RIGHT";
const string SERVER_PICK_UP = "105 GET MESSAGE";
const string SERVER_LOGOUT = "106 LOGOUT";
const string SERVER_KEY_REQUEST = "107 KEY REQUEST";
const string SERVER_OK = "200 OK";
const string SERVER_LOGIN_FAILED = "300 LOGIN FAILED";
const string SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR";
const string SERVER_LOGIC_ERROR = "302 LOGIC ERROR";
const string SERVER_KEY_OUT_OF_RANGE_ERROR = "303 KEY OUT OF RANGE";
const string CLIENT_RECHARGING = "RECHARGING";
const string CLIENT_FULL_POWER = "FULL POWER";
const string END_SIGNAL = "\a\b";

const int BUFFER_SIZE = 1024;
const int END_SIGNAL_LEN = 2;
const int MAX_USERNAME_LEN = 18;
const int MAX_SECRET_MESSAGE_LEN = 98;
const int MAX_CONFIRMATION_KEY_LEN = 5;
const int MAX_MOVEMENT_MESSAGE_LEN = 10;

const pair<int, int> AUTH_KEYS[5]{
        make_pair(23019, 32037),
        make_pair(32037, 29295),
        make_pair(18789, 13603),
        make_pair(16443, 29533),
        make_pair(18189, 21952),
};

const string move_over_obstacle_plot_right[4] = {
        SERVER_TURN_LEFT,
        SERVER_MOVE,
        SERVER_TURN_RIGHT,
        SERVER_MOVE
};

const string move_over_obstacle_plot_left[4] = {
        SERVER_TURN_RIGHT,
        SERVER_MOVE,
        SERVER_TURN_LEFT,
        SERVER_MOVE
};

#endif //BIE_PSI_CONSTANTS_H
